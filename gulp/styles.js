'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;
var _ = require('lodash');

gulp.task('styles', function () {
    var lessOptions = {
        paths: [
            conf.paths.src
        ]
    };


    return gulp.src([
        path.join(conf.paths.src, '/less/app.less')
    ])
        .pipe($.sourcemaps.init())
        .pipe($.less()).on('error', conf.errorHandler('Less'))
        .pipe($.autoprefixer()).on('error', conf.errorHandler('Autoprefixer'))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(path.join(conf.paths.dist, '/css/')))
        .pipe(browserSync.reload({ stream: true }));
});