$(document).ready(function () {

    $('.date').mask('00/00/0000');
    $('.phone_with_ddd_sp').mask('(00) Z0000-0000', {
        translation: {
            'Z': {
                pattern: /[0-9]/,
                optional: true
            }
        }
    });

    $('.cpf').mask('000.000.000-00');
    $('.cep').mask('00000-000');
    $('.short_date').mask('00 / 00');
    $('.card_code').mask('000');
    $('#card-number').mask('0000 0000 0000 0000');

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        el = e.target || e.currentTarget;

        if($(el).hasClass('validate')){
            var fields = [
                'nome_completo',
                'email',
                'telefone',
                'cpf',
                'endereco',
                'numero',
                'nascimento'
            ];

            if(!validateForm(fields)){
                return;
            }
        }

        var $active = $('#shop-wizard li.active');
        $active.next().removeClass('disabled');

        $($active).next().find('a[data-toggle="tab"]').click();

    });



    var validateForm = function(fields){

        var isValid = true;

        for(var i=0;i<fields.length;i++){
            var field = $("#" + fields[i]);

            if(field.attr('required') && field.val().length === 0 || field.val() === ''){
                isValid = false;
                field.focus();
                toastr.warning('Por favor, informe os dados corretamente.', 'Oooops!', {timeOut: 10000})
                break;
            }
        }

        return isValid;

    };

    var products = {
        1:{
            nome: "GENIO II BLACK 110V - AUTOMÁTICA",
            descricao: "Sua primeira NESCAFÉ Dolce Gusto automática. Prepara sua bebida na medida certa.",
            preco: "R$749,90",
            imagem: "img/mini-me-black-final1.png"
        },
        2:{
            nome: "PORTA CÁPSULAS RENOVE",
            descricao: "O porta capsulas Renove tem espaço para 16 capsulas e é feito com materiais 100% reciclados, sendo 15% do produto composto por resíduos de cápsulas Nescafé® Dolce Gusto® pós uso.",
            preco: "R$49,90",
            imagem: "img/portacapsreciclavel_4.png"
        },
        3:{
            nome: "ESPRESSO",
            descricao: "Nosso clássico e rico Espresso.",
            preco: "R$22,20",
            imagem: "img/11407_espresso_1.png"
        }
    };

    $("input[name='product']").click(function(e){

        var el = e.target || e.currentTarget;

        if(el.value && el.value > 0){
            updateProductPanel(products[el.value]);
            $('#select-product').find('.next-step').removeClass('disabled');
        }
    });

    function updateProductPanel(product){
        $("#lbl-product-img").attr('src', product.imagem);
        $("#lbl-product-price").html(product.preco);
        $("#lbl-product-desc").html(product.descricao);
        $("#lbl-product-name").html(product.nome);
    }

    $('#proceed-checkout').click(function(e){

        var fields = [
            'card-number',
            'card-expiration',
            'card-code'
        ];

        if(!validateForm(fields)){
            return;
        }

        var formData = {
            "produto": products[$("input[name='product']:checked").val()],
            "nome_completo": $("#nome_completo").val(),
            "telefone": $("#telefone").val(),
            "cpf": $("#cpf").val(),
            "nascimento": $("#nascimento").val(),
            "endereco": {
                "rua": $("#endereco").val(),
                "numero": $("#numero").val(),
                "estado": $("#estado").val(),
                "cidade": $("#cidade").val(),
                "complemento": $("#complemento").val(),
                "cep": $("#cep").val()
            },
            "pagamento": {
                "cardNumber": $("#card-number").val(),
                "cardExpiration": $("#card-expiration").val(),
                "cardCode": $("#card-code").val()
            }
        };

        $.ajax({
            type: 'post',
            url:'http://59bf1f8c359bf20011675527.mockapi.io/api/v1/buy',
            dataType: 'json',
            data: formData,
            success: function(response){
                if(response.id && !response.error){
                    var $active = $('#shop-wizard li.active');
                    $active.next().removeClass('disabled');
                    $($active).next().find('a[data-toggle="tab"]').click();

                    $('#shop-wizard li').addClass('disabled');
                } else {
                    $('#card-number').focus();
                    $('#card-code').val("");
                    toastr.error('Não foi possível concluir a transação. Por favor verifique os dados e tente novamente.', 'Um erro inesperado aconteceu!', {timeOut: 10000});
                }
            }
        });
    });
});