#Instruções de instalação.

Você vai precisar ter  Node.js instalado, juntamente com o bower e o npm nas verões mais recentes.

Clone o repositório para a sua máquina local.

``git clone https://vagnerleitte@bitbucket.org/vagnerleitte/allsetchallenge.git``

Em seguida entre no diretório *_allsetchallenge_*.

``cd allsetchallenge``

Excecute os comandos de instalação do bower e npm para instalar as bibliotecas necessárias.

``bower install``

``npm install``

Para executar o código, execute o seguinte comando.

``gulp dev``

Assim que os arquivos forem processados e compilados, o servidor será iniciado automaticamente e abrirá a aplicação no navegador padrão.
Caso o navegador não abra automaticamente, por favor, abra o seguinte link no seu navegador: [http://localhost:3000](http://localhost:3000)


Enjoy
Vagner Leitte