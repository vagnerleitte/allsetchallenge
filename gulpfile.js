var gulp = require('gulp');
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var path = require('path');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var gutil = require('gulp-util');
var conf = require('./gulp/conf');
var $ = require('gulp-load-plugins')();
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');

gulp.task('less', function() {

    var lessOptions = less({
        paths: [
            path.join(__dirname, 'less', 'includes')
        ]
    });

    return gulp.src('src/less/app.less')
        .pipe(sourcemaps.init())
        .pipe($.less(lessOptions)).on('error', conf.errorHandler('Less'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css'));

});


gulp.task('scripts', function() {
    gulp.task('scripts', function() {
        gulp.src([
            './bower_components/jquery/dist/jquery.js',
            './bower_components/jquery-mask-plugin/dist/jquery.mask.js',
            './bower_components/jquery-validation/dist/jquery.validate.js',
            './bower_components/jquery-validation/dist/additional-methods.js',
            './bower_components/bootstrap/dist/js/bootstrap.js',
            './bower_components/toastr/toastr.js',
            './src/js/app.js'
        ])
            .pipe(concat('app.js'))
            .pipe(gulp.dest('./public/js/'));

    });
});

/***
 Copying assest to dist folder

 **/

gulp.task('copy-assets', function() {

    gulp.src([
        './bower_components/bootstrap/fonts/**/*'
    ]).pipe(gulp.dest('public/fonts'));

    /**
     * Copying images
     **/
    gulp.src([
        'src/img/**/*'
    ])
        .pipe(gulp.dest('public/img'));
});

/***
 Copying assest to dist folder

 **/

gulp.task('copy-html', function() {

    /**
     *  Copying web-fonts
     **/

    gulp.src([
        'src/**/*.html'
    ])
        .pipe(gulp.dest('public/'));
});




gulp.task('dev', ['less', 'copy-assets', 'scripts', 'copy-html'], function() {
    browserSync({
        server: {
            baseDir: 'public'
        },
        open: true,
        cors: true
    });

    gulp.watch(['src/img/**/*'], ['copy-assets']);
    gulp.watch('src/less/**/*.less', ['less']);
    gulp.watch('src/app.less', ['less']);
    gulp.watch('src/js/**/*.js', ['scripts']);
    gulp.watch(['css/**/*.css', 'scripts/**/*.js'], { cwd: 'public' }, reload);
    gulp.watch(['src/*.html'], ['copy-html']);
});


gulp.task('default', ['scripts', 'less', 'copy-assets', 'copy-html'], function() {
    browserSync({
        server: {
            baseDir: 'public'
        },
        open: false
    });
});